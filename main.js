// Знайти всі параграфи на сторінці та встановити колір фону #ff0000 //

let allParagraphs = document.querySelectorAll("p");
console.log(allParagraphs);
for (let paragraph of allParagraphs) {
    paragraph.style.backgroundColor = "#ff0000";
}

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та 
//вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let optionsList = document.getElementById("optionsList");
console.log(optionsList);

let parentElementOfOptionsList = optionsList.parentElement;
console.log(parentElementOfOptionsList);

let childNodesOfOptionsList = optionsList.childNodes;
for (node of childNodesOfOptionsList) {
    console.log(node, typeof(node));
    console.log(`${node} : ${typeof(node)}`);
}

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph

let testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";

// Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному 
// з елементів присвоїти новий клас nav-item.

let mainHeader = document.querySelector(".main-header");

let mainHeaderChildren = mainHeader.children;

for (let child of mainHeaderChildren){
    child.classList.add("nav-item");
    console.log(child);
}

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let sectionTitle = document.querySelectorAll(".section-title");
for (title of sectionTitle) {
    title.classList.remove("section-title");
    console.log(title);
}
